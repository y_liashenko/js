/** @global PAGE FORMATS*/
var pageSizes = ['A4','A5'];

/**
 * Represent Printer class
 * @param {name, paperLimit, tonerLimit} name and device limits
 * @constructor
 */
function Printer (name, paperLimit, tonerLimit){
    this.name = name;
    this.sheetsInTray = 0;
    this.cartridgeToner = tonerLimit;
    this.printedSheets = 0;
    this.pageSizeA4Count = 0;
    this.pageSizeA5Count = 0;

    /**
     * @param {number} papaer and toner limits of the device
     */
    var limits = {paperLimit:paperLimit, tonerLimit:tonerLimit};

    /**
     * @param {number} Loading paper to tray
     */
    this.addPaperToTray = function(value) {
        if ( (this.sheetsInTray + value) > limits.paperLimit) {
            throw 'You cannot get more paper!';
        }
        if (this.sheetsInTray < 0) {
            throw 'Put the paper in tray!';
        }
        this.sheetsInTray += value;
    };

    /**
     * @param {number} Cheking toner in cartridge
     */
    this.fillCartridgeWithToner = function(value) {
        if (value <= 0) {
            throw 'Please, refill cartridge !'
        }
        if (value > limits.tonerLimit) {
            throw 'Too much toner'
        }
        this.cartridgeToner = value;
    };

    /** print act
     * @param {string} paper format
     * @param {number} amount of printed pages
     */
    this.print = function(pageSize, pagesNumber){
        var knownFormat = pageSizes.indexOf(pageSize) > -1;

        if(knownFormat) {
            for(var i=0;i<pagesNumber;i++) {
                if(this.cartridgeToner <= 0) {
                    throw 'fill or replace cartridge!'
                }
                if (this.sheetsInTray <= 0) {
                    throw 'Fill paper in tray'
                }
                if (pageSize == 'A4') {
                    this.pageSizeA4Count ++;
                }
                if (pageSize == 'A5') {
                    this.pageSizeA5Count ++;
                }

                this.printedSheets++;
                this.sheetsInTray--;
                this.cartridgeToner -= 1;
            }
            /**
             * Returns message after print act
             * @param pagesNumber
             * @param pageSize
             * @returns {String}
             */
            return 'Document '+ pagesNumber +' pages is printed on ' + pageSize + ' paper';
        } else {
            throw 'I do not know this format of paper! Sorry!'
        }
    };

    /** Call report act to know number of printed pages and paper format
     */
    this.report = function() {
        return 'Printer report : ' + this.printedSheets + ' - sheets prints; ' + this.sheetsInTray + ' - sheets in tray; ' + this.pageSizeA4Count + ' - A4 format pages;' + this.pageSizeA5Count + ' - A5 format pages;';
    }
}