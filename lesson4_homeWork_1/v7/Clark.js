/**
 * Represent Clark class
 * @param {name} Clark name
 */
function Clark(name) {
    this.name = name;
    var printer = null;

    this.setPrinter = function (device) {
        printer = device;
    };

    this.print = function(pageSize, pagesNumber){
        if(!printer){
            throw 'Give me a coffee machine first!'
        }
        var message = printer.print(pageSize, pagesNumber);
        console.log(name + ' printed a document. ' + message);
    }
}