var officePrinter = new Printer('konica',250,3000);
officePrinter.addPaperToTray(100);
console.log(officePrinter.sheetsInTray + ' sheets in tray');

var clark = new Clark('Tom');

clark.setPrinter(officePrinter);
clark.print('A4', 12);

console.log(officePrinter.report());

var clark1 = new Clark('John');

clark1.setPrinter(officePrinter);
clark1.print('A5', 2);

console.log(officePrinter.report());