app.controller('TodoController', function($scope){
    $scope.list = [];

    $scope.add = function(){
        $scope.list.push({
            title: $scope.title,
            completed: false
        });
    };

    $scope.delete = function(item){
        var index = $scope.list.indexOf(item);
        if(index > -1) {
            $scope.list.splice(index, 1);
        }
    };
});