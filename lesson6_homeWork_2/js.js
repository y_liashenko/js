$(document).ready(function() {
    /** @global variables*/
    var listLine = $('li.list').html(),
        parentListLine = $('.list-block'),
        body = $("body");

    /**
     * Create init function
     */
    var
        init = function() {
            /**
             * Create handlers
             */
            body.on('click', '.btn-add', addBtnClickHandler);
            body.on('click', '.btn-delete', deleteBtnClickHandler);
            body.on('change', 'input[type=checkbox]', inputChangeHandler);
            body.on('click', '.btn-edit', buttonEditHandler);
            body.on('click', '.btn-apply', buttonApplyHandler);
            body.on('click', '.btn-hide-show', buttonHideShowHandler);
        },
        addBtnClickHandler = function() {
            $(parentListLine).append('<li class="list">' + listLine + '</li>');
        },
        deleteBtnClickHandler = function() {
            $(this).parent().hide();
        },
        inputChangeHandler = function() {
            if ( this.checked ){
                $(this).siblings('span.list-text').css('text-decoration','line-through');
            } else {
                $(this).siblings('span.list-text').css('text-decoration','none');
            }
        },
        buttonEditHandler = function() {
            var self = $(this),
                editListText = self.siblings('.list-text').text();
            self.siblings('.list-text').hide();
            self.siblings('input[type=checkbox]').after('<input type="text" />');
            self.siblings('input[type=text]').attr('value', editListText);
            self.addClass("hide");
            self.closest("li").find(".btn-apply").removeClass('hide');
        },
        buttonApplyHandler = function() {
            var self = $(this),
                applyInputText = self.siblings('input[type=text]').val();
            self.siblings('input[type=text]').remove();
            self.siblings('.list-text').text(applyInputText).show();
            self.addClass("hide");
            self.closest("li").find(".btn-edit").removeClass('hide');
        },
        buttonHideShowHandler = function() {
            var inputCheckbox = $('input[type=checkbox]');
            if ($(this).text() == 'hide') {
                for (var i=0;i<inputCheckbox.length;i++) {
                    if (inputCheckbox[i].checked){
                        $(inputCheckbox[i]).parent().addClass('hide');
                    }
                }
                $(this).text('show');
            } else if ($(this).text() == 'show'){
                $(this).text('hide');
                $('.list').removeClass('hide');
            }
        };
    if (!parentListLine.length) {
        return;
    }
    init();
});